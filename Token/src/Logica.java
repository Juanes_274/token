
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;

public class Logica {

    String tokenG;
    ArrayList<Usuario> usuarios = new ArrayList<>();
    public Scanner leerDato = new Scanner(System.in);

    public void menu() throws NoSuchAlgorithmException {
        crearUsuario();
        int opc;
        boolean salir = false;
        while (!salir) {
            System.out.println("\n\n***** M E N U   D E   O P C I O N E S *****\n");
            System.out.println("[1]. Iniciar sesion");
            System.out.println("[2]. Registrarse");
            System.out.print("[3]. Salir\n");
            System.out.print("Ingrese una opcion: ");
            opc = leerDato.nextInt();
            switch (opc) {
                case 1:
                    iniciarSesion();
                    break;

                case 2:
                    registro();

                    break;

                case 3:
                    salir = true;
                    break;

                default:
                    System.out.println("!!!OPCION INCORRECTA!!!");
                    break;
            }
        }
    }

    public void crearUsuario() {
        usuarios.add(new Usuario("1000382934", "Usuario", "Camilito", "1", "Hitman", 0, "", ""));
        usuarios.add(new Usuario("1032498680", "Admin", "Christian", "2", "Redz", 0, "", ""));
        usuarios.add(new Usuario("1001168599", "Usuario", "Juanes", "3", "Hacker", 0, "", ""));
    }

    public void iniciarSesion() throws NoSuchAlgorithmException {
        boolean vali;
        String Rol = null;

        do {
            System.out.println("\n\n********* INICIAR SESION *********");
            System.out.print("\nUsuario: ");
            String usuario = leerDato.next();
            System.out.print("Contraseña: ");
            String contraseña = leerDato.next();
            vali = validar(usuario, contraseña);
            if (vali == false) {
                System.out.println("!!! Usuario o contraseña incorrectos !!!");
            }
        } while (vali == false);

        for (Usuario usuario : usuarios) {
            if (tokenG == usuario.getToken()) {
                Rol = usuario.getRol();
            }
        }

        if (Rol == "Usuario") {
            menuUsuario();
        } else {
            menuAdmin();
        }

    }

    public void menuUsuario() {
        int opc;
        boolean salir = false;

        Usuario usu = null;

        for (Usuario usuario : usuarios) {
            if (usuario.getToken().equals(tokenG)) {
                usu = usuario;
            }
        }

        while (!salir) {
            System.out.println("\n\n***** M E N U   DE   U S U A R I O S *****\n");
            System.out.println("[1]. Consignar");
            System.out.println("[2]. Retirar");
            System.out.println("[3]. Consultar Saldo");
            System.out.println("[4]. Info Personal");
            System.out.print("[5]. Salir\n");
            System.out.print("Ingrese una opcion: ");
            opc = leerDato.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("*** CONSIGNAR ***");
                    consignar(usu);
                    break;

                case 2:
                    System.out.println("*** RETIRAR ***");
                    retirar(usu);
                    break;

                case 3:
                    System.out.println("*** SALDO ACTUAL ***");
                    System.out.println("Saldo ->" + usu.getSaldo());
                    break;

                case 4:
                    System.out.println(usu.toString());
                    break;

                case 5:
                    salir = true;
                    break;

                default:
                    System.out.println("!!!OPCION INCORRECTA!!!");
                    break;
            }
        }

    }

    public void consignar(Usuario usuario) {
        System.out.println("Valor a consignar: ");
        double nuevoSaldo = leerDato.nextDouble();
        usuario.setSaldo(usuario.getSaldo() + nuevoSaldo);
    }

    public void retirar(Usuario usuario) {
        double valorRetirar = 0;
        do {
            System.out.println("Valor a retirar: ");
            valorRetirar = leerDato.nextDouble();
            if (valorRetirar > usuario.getSaldo()) {
                System.out.println("!!! El valor a retirar es mayor que el saldo existente !!!");
            }
        } while (usuario.getSaldo() < valorRetirar);
        usuario.setSaldo(usuario.getSaldo() - valorRetirar);
    }


    public void menuAdmin() {
        int opc;
        boolean salir = false;
        while (!salir) {
            System.out.println("\n\n***** M E N U   DE   A D M I N *****\n");
            System.out.println("[1]. Ver usuarios");
            System.out.println("[2]. Eliminar Usuario");
            System.out.print("[4]. Salir\n");
            System.out.print("Ingrese una opcion: ");
            opc = leerDato.nextInt();
            switch (opc) {
                case 1:
                    System.out.println("********************************  U S U A R I O S ********************************");
                    for (Usuario usuario : usuarios) {
                        System.out.println(usuario.toString());
                        System.out.println("_________________________________________________________________________________");
                    }
                    break;

                case 2:
                    System.out.println("*** ELIMINAR USUARIO ***");
                    eliminar();
                    break;

                case 4:
                    salir = true;
                    break;

                default:
                    System.out.println("!!!OPCION INCORRECTA!!!");
                    break;
            }
        }
    }

    public void eliminar() {
        boolean existe = false;
        do {
            System.out.print("\nNombre del usuario a eliminar: ");
            String nombre = leerDato.next();
            System.out.print("Id del usuario a eliminar: ");
            String Id = leerDato.next();
            for (Usuario usuario : usuarios) {
                if (usuario.getNombre().equals(nombre)) {
                    if (usuario.getId().equals(Id)) {
                        usuarios.remove(usuario);
                        existe = true;
                        break;
                    }
                }
            }
            if (existe == false) {
                System.out.println("!!! No se encontro al usuario !!!");
            }
        } while (existe == false);

    }

    public boolean validar(String nombreUsu, String contraseña) throws NoSuchAlgorithmException {

        boolean existe = false;

        for (Usuario usuario : usuarios) {
            if (usuario.getNombreUsuario().equals(nombreUsu)) {
                if (usuario.getContraseña().equals(contraseña)) {
                    conversion(usuario);
                    existe = true;
                }
            }
        }
        return existe;
    }

    public void conversion(Usuario usuario) throws NoSuchAlgorithmException {

        String union = usuario.getNombre() + usuario.getNombreUsuario() + usuario.getRol() + usuario.getId() + usuario.getContraseña();
        String firma = Base64.getEncoder().encodeToString(union.getBytes());
        String union2 = union + firma;

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(union2.getBytes(StandardCharsets.UTF_8));
        String token = DatatypeConverter.printHexBinary(digest).toLowerCase();
        usuario.setFirma(firma);
        usuario.setToken(token);

        tokenG = token;
    }

    public void registro() {
        Usuario unUsuario;
        String nombre, id, nombreUsu, rol, contraseña;
        System.out.print("Ingrese el nombre: ");
        nombre = leerDato.next();
        System.out.print("Ingrese el Nickname: ");
        nombreUsu = leerDato.next();
        System.out.print("Ingrese el id: ");
        id = leerDato.next();
        System.out.print("Ingrese el Contraseña: ");
        contraseña = leerDato.next();
        unUsuario = new Usuario(id, "usuario", nombre, contraseña, nombreUsu, 0, "", "");
        usuarios.add(unUsuario);

    }
}
