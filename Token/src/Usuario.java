
public class Usuario {

    public String id;
    public String rol;
    public String nombre;
    public String contraseña;
    public String nombreUsuario;
    public double saldo;
    public String firma;
    public String token;

    public Usuario(String id, String rol, String nombre, String contraseña, String nombreUsuario, double saldo, String firma, String token) {
        this.id = id;
        this.rol = rol;
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.nombreUsuario = nombreUsuario;
        this.saldo = saldo;
        this.firma = firma;
        this.token = token;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getId() {
        return id;
    }

    public String getRol() {
        return rol;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getFirma() {
        return firma;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    @Override
    public String toString() {
        System.out.println("   id\t\t\tRol\t\tnombre\t\tcontraseña\tnombreUsuario");

        return "   " + id
                + "\t\t" + rol
                + "\t\t" + nombre
                + "\t" + contraseña
                + "\t\t" + nombreUsuario;
    }

}
