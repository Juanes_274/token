
import java.security.NoSuchAlgorithmException;


/**
 *
 * @author Juan Esteban Arias
 * @author Juan Camilo Hernandez
 * @author Christian David Jimenez
 */
public class Launcher {
    
    public static void main(String[] args) throws NoSuchAlgorithmException {
        new Logica().menu();
    }
}
